using OpenQA.Selenium;

namespace web_test.SeleniumTests;

public class Helper
{
    public static void takeSnapShot(IWebDriver webdriver,String fileWithPath) 
    {
        Screenshot ss = ((ITakesScreenshot)webdriver).GetScreenshot();
        ss.SaveAsFile(fileWithPath, ScreenshotImageFormat.Png);
    }
}