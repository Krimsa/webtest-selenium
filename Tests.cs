using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;

namespace web_test.SeleniumTests;

public class Tests
{
    private static readonly string screenshotFolder = Directory.GetCurrentDirectory() + "../../../../screenshots/";
    private static readonly Uri remoteDriverURI = new Uri(GetRemoteDriverURI());
    private static readonly DriverOptions driverOptions = GetDriverOptions();

    private static string GetRemoteDriverURI()
    {
        return Environment.GetEnvironmentVariable("RemoteDriverURI") ?? "http://localhost:4444";
    }

    private static DriverOptions MatchBrowser(string browser) => browser switch
    {
        "chrome" => new ChromeOptions(),
        "firefox" => new FirefoxOptions(),
        "edge" => new EdgeOptions(),
        null => new ChromeOptions(),
        _ => throw new ArgumentException($"Browser '{browser}' is not known"), //_ это переменная на которую не сослаться
    };

    private static DriverOptions GetDriverOptions()
    {
        var browser = Environment.GetEnvironmentVariable("BrowserType"); //в консоли(без sudo) пишем: export BrowserType="..."
        return MatchBrowser(browser);
    }

    [Test] //1
    public void TestDotnetGetStarter()
    {
        using var driver = new RemoteWebDriver(remoteDriverURI, driverOptions.ToCapabilities());
        driver.Navigate().GoToUrl("https://dotnet.microsoft.com/");
        driver.FindElement(By.LinkText("Get Started")).Click();
        IWebElement nextLink = null;
        do
        {
            nextLink?.Click(); //проверка на null?
            nextLink = driver.FindElements(By.CssSelector(".step:not([style='display:none;']):not([style='display: none;']) .step-select")).FirstOrDefault();
        } while (nextLink != null);
        var lastStepTitle = driver.FindElement(By.CssSelector(".step:not([style='display:none;']):not([style='display: none;']) h2")).Text;
        Helper.takeSnapShot(driver, screenshotFolder + "/test1.png");
        Assert.AreEqual(lastStepTitle, "Next steps");
    }

    [Test] //2
    public void TestYandexSearch()
    {/**/
        using var driver = new RemoteWebDriver(remoteDriverURI, driverOptions.ToCapabilities());
        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3); //даём время на нахождение элементов
        driver.Navigate().GoToUrl("https://yandex.ru/");
        IWebElement query = driver.FindElement(By.Id("text"));
        query.SendKeys("Get started dotNET");
        query.SendKeys(Keys.Enter);
        IWebElement correctResult = driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div[1]/div[1]/ul/li[1]/div/div[1]/a/h2/span"));
        Helper.takeSnapShot(driver, screenshotFolder + "/test2.png");
        Assert.NotNull(correctResult);
    }
}

/* 
  IWebElement correctResult = driver.FindElement(By.XPath("//*[text()='Get started with .NET - Microsoft Docs']"));
*/
/**/